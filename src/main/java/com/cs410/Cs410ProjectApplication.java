package com.cs410;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cs410ProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cs410ProjectApplication.class, args);
	}

}
