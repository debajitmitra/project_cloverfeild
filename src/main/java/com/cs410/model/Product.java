package com.cs410.model;

import java.util.List;

public class Product {
	
	private String prodId;
	private String productName;
	private String description;
	private String brand;
	private String rootCategory;
	private String childCategory;
	private String imageUrl;
	private int recommendedProductCount;
	private List<Product> recommendedProds;
	
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getRootCategory() {
		return rootCategory;
	}
	public void setRootCategory(String rootCategory) {
		this.rootCategory = rootCategory;
	}
	public String getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<Product> getRecommendedProds() {
		return recommendedProds;
	}
	public void setRecommendedProds(List<Product> recommendedProds) {
		this.recommendedProds = recommendedProds;
	}

	public int getRecommendedProductCount() {
		return recommendedProductCount;
	}

	public void setRecommendedProductCount(int recommendedProductCount) {
		this.recommendedProductCount = recommendedProductCount;
	}
}
