package com.cs410.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.cs410.model.Product;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.util.NamedList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class ProductController {

	@GetMapping("/product/{productId}")
	@ResponseBody
	public Product getProductDetails(@PathVariable String productId) throws SolrServerException, IOException {
		SolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/e_store").build();
		SolrQuery solrQuery = new SolrQuery();
		//solrQuery.setFields("uniq_id");
		String key = "uniq_id:";
		solrQuery.setQuery(key+productId);
		solrQuery.setMoreLikeThis(true);
		QueryResponse response = solrClient.query(solrQuery);
		Product product = null;
		List<Product> recommendedProds = new ArrayList<Product>();
		if (response != null) {
			for (int i = 0; i<response.getResults().size(); i++) {
				product = populateProduct(response.getResults().get(i));
				NamedList<SolrDocumentList> recommendedProdsList = response.getMoreLikeThis();
				if (recommendedProdsList != null) {
					SolrDocumentList docList = recommendedProdsList.get(productId);
					if (docList != null) {
						product.setRecommendedProductCount(docList.size());
						docList.forEach(t -> {
							recommendedProds.add(populateProduct(t));
						});
					}
				}
			}
		}
		product.setRecommendedProds(recommendedProds);
		return product;
	}

	private Product populateProduct(SolrDocument solrDocument) {
		Product product;
		product = new Product();
		product.setProdId((String)solrDocument.getFieldValue("uniq_id"));
		product.setProductName((String)solrDocument.getFieldValue("product_name"));
		product.setDescription((String)solrDocument.getFieldValue("description"));
		product.setBrand((String)solrDocument.getFieldValue("brand"));
		product.setImageUrl((String)solrDocument.getFieldValue("primary_image_url"));
		product.setRootCategory((String)solrDocument.getFieldValue("parent_category"));
		product.setChildCategory((String)solrDocument.getFieldValue("child_category"));
		return product;
	}

	@GetMapping("/products")
	@ResponseBody
	public List<Product> getProducts(@RequestParam String searchTerm) throws SolrServerException, IOException {

		SolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/e_store").build();
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setQuery(searchTerm);
		solrQuery.setMoreLikeThis(true);
		QueryResponse response = solrClient.query(solrQuery);
		List<Product> products = new ArrayList<Product>();
		Product product = null;
		if (response != null) {

			for (int i = 0; i<response.getResults().size(); i++) {
				product = populateProduct(response.getResults().get(i));
				products.add(product);
			}
		}

		return products;

	}
	
	
	@GetMapping("/autoSuggestions")
	@ResponseBody
	public List<String> getAutoSuggestions(@RequestParam String searchTerm) throws SolrServerException, IOException {
		SolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/e_store").build();
		SolrQuery solrQuery = new SolrQuery();
		//solrQuery.setFields("uniq_id");
		String key = "product_name_suggest:";
		solrQuery.setQuery(key+searchTerm);
		QueryResponse response = solrClient.query(solrQuery);
		List<String> autoSuggestions = new ArrayList<String>();
		if (response != null) {
			for (int i = 0; i<response.getResults().size(); i++) {
				
				autoSuggestions.add((String) response.getResults().get(i).getFieldValue("product_name_suggest"));
			}
		}
		
		return autoSuggestions;
	}

}
