# Follow the below steps to setup and run the application


## Installation

**Step1.** Clone the project.

```bash
git clone https://lab.textdata.org/spj3/Course_Project.git
```
After pulling the code from the repository go to Course_Project root directory and solr-7.7.2/bin folder and run the below command to start the Solr server.
```bash
cd Course_Project
cd solr-7.7.2/bin
./solr start
```
**Step2.** Ingest the product catalog data to the Solr by running the below command. The product catalog data file is there in bin folder itself. We got the product catalog data from Kaggle and cleansed the data for our requirement. The product catalog data file is there in bin folder itself.
```bash
./post -c e_store -type text/csv flipkart_com-ecommerce_sample.csv
```
To simplify the project setup, we already indexed the data and put the indexed data in the repository. So, you no need to perform **step 2.** But if required you can go ahead and do the indexing by following **step2.**

All the solr config and schema files are there under the solr core. You can find the same under **Course_Project/solr-7.7.2/server/solr/e_store**

In this application we have built the below features.
- **Auto suggestions**
- **Product search**
- **Recommendations for a given product based on content filtering.**

To demonstrate the features implemented in **step5**, we have created a web application that accesses the REST endpoints to get the data from the Solr. We used SolrJ to interact with the Apache Solr. Exposed below REST endpoints.
- **API to get autosuggestions**
- **API to get the product catalog search results**
- **API to get the product details and recommendation**

**Step3.** Go to **Course_Project** root directory. To access the web application, start the application by running the below command. The server will start by default on 8081 port.
```bash
cd Course_Project
java -jar ./cs410-project-0.0.1-SNAPSHOT.war
```
**Step4.** Once the server is started, hit the URL **http://localhost:8081/** to load the e-store front end application.

The user can search for any product by using the search box. Users can leverage the auto-suggestion feature. Try to use the below search terms to get recommendations for the products as we have limited products in the product catalog, not all the products have recommendations.

- **cycling shorts**
- **shoes**
- **running shoes**
- **analog watch**
- **boots**
- **wedding ring**
- **diamond ring**

**Step5.** The user can search for any product by using the search box. Users can leverage the auto-suggestion feature.
![Auto Suggestions](https://lab.textdata.org/spj3/Course_Project/blob/master/Picture1.png)

**Step6.** Users will get the search results based on the search term.
![Product Catalog Search](https://lab.textdata.org/spj3/Course_Project/blob/master/Picture2.png)

**Step7.** Once the user selects any product from the search results user will land on the product details page where the user can see the full product details along with the recommendations.
![Product details with recommendations](https://lab.textdata.org/spj3/Course_Project/blob/master/Picture3.png)
## The presentation Project Clover Field - A Recommendation System.pptx is available under Course_Project folder.
